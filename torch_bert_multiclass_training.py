import os
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import torch
from torch.utils.data import DataLoader, Dataset
from torch.nn import functional as F
from transformers import AutoModel, AutoTokenizer, AdamW, get_linear_schedule_with_warmup
from tqdm.notebook import tqdm
from collections import Counter
import re
import string
import nltk
from nltk.corpus import stopwords
from sklearn import model_selection

# Check for GPU availability
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f"Using device: {device}")

model_name = 'bert-base-multilingual-cased'
n_epochs = 5
max_len = 128
batch_size = 32

frame = pd.read_csv('/content/TATACLIQ_21_06_2022_NEW (1).csv')
train = frame.sample(frac=1)

train['Label'] = train['Label'].astype(int)

test = train.sample(4500)

train['Label'].nunique()

tokenizer = AutoTokenizer.from_pretrained(model_name)

train_text = train['Trans'].values.tolist()
test_text = test['Trans'].values.tolist()

def quick_encode(values, maxlen):
    tokens = tokenizer.batch_encode_plus(values, max_length=maxlen, pad_to_max_length=True, truncation=True)
    return np.array(tokens['input_ids']), np.array(tokens['attention_mask'])

x_train, train_masks = quick_encode(train_text, max_len)
x_test, test_masks = quick_encode(test_text, max_len)
y_train = train.Label.values

class CustomDataset(Dataset):
    def __init__(self, input_ids, attention_mask, labels=None):
        self.input_ids = input_ids
        self.attention_mask = attention_mask
        self.labels = labels

    def __len__(self):
        return len(self.input_ids)

    def __getitem__(self, idx):
        item = {
            'input_ids': torch.tensor(self.input_ids[idx], dtype=torch.long),
            'attention_mask': torch.tensor(self.attention_mask[idx], dtype=torch.long)
        }
        if self.labels is not None:
            item['labels'] = torch.tensor(self.labels[idx], dtype=torch.long)
        return item

train_dataset = CustomDataset(x_train, train_masks, y_train)
test_dataset = CustomDataset(x_test, test_masks)

train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=batch_size)

class BertForMultiClass(torch.nn.Module):
    def __init__(self, model_name, num_labels):
        super(BertForMultiClass, self).__init__()
        self.bert = AutoModel.from_pretrained(model_name)
        self.dropout = torch.nn.Dropout(0.3)
        self.linear = torch.nn.Linear(self.bert.config.hidden_size, num_labels)

    def forward(self, input_ids, attention_mask):
        outputs = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        cls_output = outputs[1]  # pooler output
        cls_output = self.dropout(cls_output)
        logits = self.linear(cls_output)
        return logits

model = BertForMultiClass(model_name, num_labels=39)
model = model.to(device)

optimizer = AdamW(model.parameters(), lr=1e-5)
total_steps = len(train_loader) * n_epochs
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

criterion = torch.nn.CrossEntropyLoss()

def train_epoch(model, data_loader, optimizer, criterion, scheduler, device):
    model.train()
    losses = []
    correct_predictions = 0
    for batch in tqdm(data_loader, desc="Training"):
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['labels'].to(device)

        outputs = model(input_ids=input_ids, attention_mask=attention_mask)
        _, preds = torch.max(outputs, dim=1)
        loss = criterion(outputs, labels)

        correct_predictions += torch.sum(preds == labels)
        losses.append(loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        scheduler.step()

    return correct_predictions.double() / len(data_loader.dataset), np.mean(losses)

def eval_model(model, data_loader, criterion, device):
    model.eval()
    losses = []
    correct_predictions = 0
    with torch.no_grad():
        for batch in tqdm(data_loader, desc="Evaluation"):
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)

            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            _, preds = torch.max(outputs, dim=1)
            loss = criterion(outputs, labels)

            correct_predictions += torch.sum(preds == labels)
            losses.append(loss.item())

    return correct_predictions.double() / len(data_loader.dataset), np.mean(losses)

for epoch in range(n_epochs):
    print(f'Epoch {epoch + 1}/{n_epochs}')
    train_acc, train_loss = train_epoch(model, train_loader, optimizer, criterion, scheduler, device)
    print(f'Train loss {train_loss} accuracy {train_acc}')

    val_acc, val_loss = eval_model(model, test_loader, criterion, device)
    print(f'Validation loss {val_loss} accuracy {val_acc}')