def predict_single_string(model, tokenizer, text, device, max_len=128):
    # Tokenize the input text
    inputs = tokenizer.encode_plus(
        text,
        None,
        add_special_tokens=True,
        max_length=max_len,
        pad_to_max_length=True,
        return_token_type_ids=False,
        truncation=True
    )

    input_ids = torch.tensor(inputs['input_ids'], dtype=torch.long).unsqueeze(0).to(device)
    attention_mask = torch.tensor(inputs['attention_mask'], dtype=torch.long).unsqueeze(0).to(device)

    # Put model in evaluation mode
    model.eval()
    with torch.no_grad():
        outputs = model(input_ids=input_ids, attention_mask=attention_mask)
        _, prediction = torch.max(outputs, dim=1)

    return prediction.item()

# Example usage:
text = "Example text to classify"
predicted_label = predict_single_string(model, tokenizer, text, device)
print(f'Predicted label: {predicted_label}')